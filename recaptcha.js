var reCaptcha = {

	siteKey: '',
	htmlElement: '',
	verifyCallback: function(){},
	expiredCallback: function(){},
	
	reset: function(){
		grecaptcha.reset();
		reCaptcha.expiredCallback();
	},
	
	init: function (siteKey, htmlElement, verifyCallback, expiredCallback) {

		// site key given to us by Google for this specific site's reCaptcha
		this.siteKey = siteKey;

		// html element we are appending the reCaptcha to
		this.htmlElement = htmlElement;
		
		// overwrite these functions with the desired behavior for each callback if they are passed in
		if (typeof verifyCallback === "function") {
			this.verifyCallback = verifyCallback;
		}
		
		if (typeof expiredCallback === "function") {
			this.expiredCallback = expiredCallback;
		}

		// start adding the reCaptcha in, load the script from Google
		this.addElement();
		this.getScript();
	},
	
	// add the html element to the specified element, htmlElement, note: can NOT be have an id of 'recaptcha_element'
	addElement: function() {
		jQuery('#'+this.htmlElement).after('<form action="?" method="POST"><div id="recaptcha_element"></div></form>');
	},

	// get Google ReCaptcha script
	getScript: function() {
		jQuery.getScript("https://www.google.com/recaptcha/api.js?onload=reCaptchaOnloadCallback&render=explicit", function(data, textStatus, jqxhr) {
			// 
		});
	},
	
	// renders the reCaptcha
	renderReCaptcha: function() {
		grecaptcha.render('recaptcha_element', {
			'sitekey' : reCaptcha.siteKey,
			'callback' : reCaptcha.verifyCallback,
			'expired-callback': reCaptcha.reset
		});
	},
	
	isCaptchaChecked: function() {
		return grecaptcha && grecaptcha.getResponse().length !== 0;
	}
	
};

function reCaptchaOnloadCallback () {
	reCaptcha.renderReCaptcha();
}